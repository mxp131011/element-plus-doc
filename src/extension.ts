import * as vscode from 'vscode';
import { useDocLink, useRegisterDocumentLinkProvider, useRegisterHoverProvider } from '@/extension/index';
import { getUnderline } from '@/utils/global';

/**
 * 激活的入口
 */
export function activate(context: vscode.ExtensionContext) {
  console.log('插件已启用');

  let disposableList: vscode.Disposable[] = [];
  disposableList = setSubscriptions(context);

  context.subscriptions.push(
    vscode.workspace.onDidChangeConfiguration(() => {
      // 手动销毁
      disposableList.forEach((item) => {
        item.dispose();
      });
      disposableList = setSubscriptions(context);
    })
  );
}

/**
 * 销毁函数
 */
export function deactivate() {
  console.log('插件已经销毁');
}

/**
 * 注册
 */
function setSubscriptions(context: vscode.ExtensionContext): vscode.Disposable[] {
  const underline = getUnderline();

  const registerDocumentLinkProvider = useRegisterDocumentLinkProvider();
  const registerHoverProvider = useRegisterHoverProvider();

  context.subscriptions.push(registerDocumentLinkProvider);
  context.subscriptions.push(registerHoverProvider);
  // 只有当需要添加超链接时才注册
  if (underline) {
    const docLink = useDocLink();
    context.subscriptions.push(docLink);
    return [docLink, registerDocumentLinkProvider, registerHoverProvider];
  } else {
    return [registerDocumentLinkProvider, registerHoverProvider];
  }
}
