import type { TagDoc } from '@/types/tag-doc';
import selectDoc from './select';
import treeDoc from './tree';

const doc: TagDoc.TagDocInstance = {
  url: 'component/tree-select.html',
  attributes: [...(selectDoc?.attributes || []), ...(treeDoc?.attributes || [])],
  events: [...(selectDoc?.events || []), ...(selectDoc?.events || [])],
  slots: [...(selectDoc?.slots || []), ...(selectDoc?.slots || [])],
  exposes: [...(selectDoc?.exposes || []), ...(selectDoc?.exposes || [])],
  childAttributes: [...(selectDoc?.childAttributes || []), ...(selectDoc?.childAttributes || [])],
};
export default doc;
