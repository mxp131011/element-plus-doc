import type { TagDoc } from '@/types/tag-doc';

const doc: TagDoc.TagDocInstance = {
  url: 'component/segmented.html#api',
  attributes: [
    {
      name: 'model-value',
      description: { cn: '绑定值', en: 'binding value' },
      type: ['string', 'number', 'boolean'],
      default: '—',
      value: [],
    },
    {
      name: 'options',
      description: {
        cn: '选项的数据 ts 类型 ({label: string;value: string &#124; number &#124; boolean;disabled?: boolean;[key: string]: any} &#124; string &#124; number &#124; boolean)[]',
        en: 'data of the options (ts type: ({label: string;value: string &#124; number &#124; boolean;disabled?: boolean;[key: string]: any} &#124; string &#124; number &#124; boolean)[])',
      },
      type: ['array'],
      default: '—',
      value: [],
    },
    {
      name: 'size',
      description: { cn: '组件大小', en: 'size of component' },
      type: 'enum',
      default: "''",
      value: ['large', 'default', 'small', ''],
    },
    {
      name: 'block',
      description: { cn: '撑满父元素宽度', en: 'fit width of parent content' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'disabled',
      description: { cn: '是否禁用', en: 'whether segmented is disabled' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'validate-event',
      description: { cn: '是否触发表单验证', en: 'whether to trigger form validation' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'name',
      description: { cn: '原生 `name` 属性', en: "native 'name' attribute" },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'id',
      description: { cn: '原生 `id` 属性', en: "native 'id' attribute" },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'aria-label',
      description: { cn: '原生 `aria-label` 属性', en: "native 'aria-label' attribute" },
      type: 'string',
      default: '—',
      value: [],
    },
  ],
  events: [
    {
      name: 'search',
      description: { cn: '当所选值更改时触发，参数是当前选中的值', en: 'triggers when the selected value changes, the param is current selected value' },
      type: 'function',
      param: '(val: any) =&gt; void',
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '默认插槽内容', en: 'customize default content' },
    },
  ],
};
export default doc;
