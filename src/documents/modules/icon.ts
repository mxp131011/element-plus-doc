import type { TagDoc } from '@/types/tag-doc';
const doc: TagDoc.TagDocInstance = {
  url: 'component/icon.html',
  attributes: [
    {
      name: 'color',
      description: { cn: 'svg 的 fill 颜色。(默认继承颜色)', en: "SVG tag's fill attribute. (default:inherit from color)" },
      type: 'string',
      default: '',
      value: [],
    },
    {
      name: 'size',
      description: { cn: 'SVG 图标的大小，size x size。(默认继承字体大小)', en: 'SVG icon size, size x size. (default:inherit from font size)' },
      type: ['string', 'number'],
      default: '',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '	自定义默认内容', en: 'customize default content' },
    },
  ],
};
export default doc;
