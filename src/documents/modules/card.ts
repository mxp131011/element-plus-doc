import type { TagDoc } from '@/types/tag-doc';

const doc: TagDoc.TagDocInstance = {
  url: 'component/card.html',
  attributes: [
    {
      name: 'header',
      description: {
        cn: '卡片的标题 你既可以通过设置 header 来修改标题，也可以通过 `slot#header` 传入 DOM 节点',
        en: 'title of the card. Also accepts a DOM passed by `slot#header`',
      },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'footer',
      description: {
        cn: '卡片页脚。 你既可以通过设置 header 来修改标题，也可以通过 slot#footer 传入 DOM 节点',
        en: 'footer of the card. Also accepts a DOM passed by slot#footer',
      },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'body-style',
      description: { cn: 'body 的 CSS 样式', en: 'CSS style of card body' },
      type: 'CSSProperties',
      default: '—',
      value: [],
    },
    {
      name: 'body-class',
      description: { cn: '卡片的自定义类名', en: 'custom class name of card body' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'shadow',
      description: { cn: '设置阴影显示时机', en: 'when to show card shadows' },
      type: 'enum',
      default: "'never'",
      value: ['always', 'never', 'hover'],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '自定义默认内容', en: 'customize default content' },
    },
    {
      name: 'header',
      description: { cn: '卡片标题内容', en: 'content of the Card header' },
    },
    {
      name: 'footer',
      description: { cn: '卡片页脚内容', en: 'content of the Card footer' },
    },
  ],
};
export default doc;
