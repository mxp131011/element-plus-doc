import type { TagDoc } from '@/types/tag-doc';
import inputDoc from './input';

const doc: TagDoc.TagDocInstance = {
  url: 'component/mention.html#api',
  attributes: [
    {
      name: 'options',
      description: {
        cn: '提及选项列表,类型为:{value: string;label?: string;disabled?: boolean [key: string]: any}[]',
        en: 'mention options list (type:{value: string;label?: string;disabled?: boolean [key: string]: any}[])',
      },
      type: ['array'],
      default: '[]',
      value: [],
    },
    {
      name: 'prefix',
      description: {
        cn: '触发字段的前缀。可以是一个字符串数组或字符串, 字符串(包含数组里的每一个字符串)长度必须且只能为 1,',
        en: 'prefix character to trigger mentions. The string length must be exactly 1',
      },
      type: ['string', 'array'],
      default: '@',
      value: [],
    },
    {
      name: 'split',
      description: { cn: '用于拆分提及的字符。 字符串长度必须且只能为 1', en: 'character to split mentions. The string length must be exactly 1' },
      type: 'string',
      default: ' ',
      value: [],
    },
    {
      name: 'filter-option',
      description: {
        cn: '定制筛选器选项逻辑,值为false 或一个函数且函数必须返回布尔值',
        en: 'customize filter option logic',
      },
      type: ['boolean', 'function(pattern: string, option: MentionOption))=> boolean'],
      default: '—',
      value: [],
    },
    {
      name: 'placement',
      description: { cn: '设置弹出位置', en: 'set popup placement' },
      type: 'string',
      default: '—',
      value: ['bottom', 'top'],
    },
    {
      name: 'show-arrow',
      description: { cn: '下拉菜单的内容是否有箭头', en: 'whether the dropdown panel has an arrow' },
      type: 'boolean',
      default: '—',
      value: [],
    },
    {
      name: 'offset',
      description: { cn: '下拉面板偏移量', en: 'offset of the dropdown panel' },
      type: 'number',
      default: '0',
      value: [],
    },
    {
      name: 'whole',
      description: {
        cn: '当退格键被按下做删除操作时，是否将提及部分作为整体删除',
        en: 'when backspace is pressed to delete, whether the mention content is deleted as a whole',
      },
      type: 'boolean',
      default: 'false',
      value: [],
    },
    {
      name: 'check-is-whole',
      description: {
        cn: '当退格键被按下做删除操作时，检查是否将提及部分作为整体删除',
        en: 'when backspace is pressed to delete, check if the mention is a whole',
      },
      type: 'function(pattern: string, prefix: string) => boolean',
      default: '—',
      value: [],
    },
    {
      name: 'loading',
      description: { cn: '提及的下拉面板是否处于加载状态', en: 'whether the dropdown panel of mentions is in a loading state' },
      type: 'boolean',
      default: '—',
      value: [],
    },
    {
      name: 'model-value',
      description: { cn: '输入值', en: 'input value' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'popper-class',
      description: { cn: '自定义浮层类名', en: 'custom class name for dropdown panel' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'popper-options',
      description: {
        cn: '自定义 popper 选项，更多请参考[popper.js](https://popper.js.org/docs/v2/)',
        en: 'Customized popper option，更多请参考[popper.js](https://popper.js.org/docs/v2/)',
      },
      type: 'object',
      default: "{modifiers: [{name: 'computeStyles',options: {gpuAcceleration: false}}]}",
      value: [],
    },
    ...inputDoc!.attributes!,
  ],

  events: [
    {
      name: 'search',
      description: { cn: '按下触发字段时触发', en: 'trigger when prefix hit' },
      type: 'function',
      param: '(pattern: string, prefix: string) =&gt; void',
    },
    {
      name: 'select',
      description: { cn: '当用户选择选项时触发', en: 'trigger when user select the option' },
      type: 'function',
      param: '(option: MentionOption, prefix: string) =&gt; void',
    },
    ...inputDoc!.events!,
  ],
  slots: [
    {
      name: 'label',
      description: {
        cn: '自定义标签内容 返回:{ item: MentionOption, index: number }',
        en: 'content as option label (return { item: MentionOption, index: number })',
      },
    },
    {
      name: 'loading',
      description: { cn: '自定义 loading内容	', en: 'content as option loading' },
    },
    {
      name: 'header',
      description: { cn: '下拉列表顶部的内容	', en: 'content at the top of the dropdown' },
    },
    {
      name: 'footer',
      description: { cn: '下拉列表底部的内容', en: 'content at the bottom of the dropdown' },
    },
    ...inputDoc!.slots!,
  ],
  exposes: [
    {
      name: 'input',
      description: { cn: '使 input 组件获得焦点', en: 'el-input component instance' },
      type: 'function',
      param: '() =&gt; Ref&#60; InputInstance &#124; null &#62;',
    },
    {
      name: 'tooltip',
      description: { cn: '使 input 组件失去焦点', en: 'el-tooltip component instance' },
      type: 'function',
      param: '() =&gt; Ref&#60; TooltipInstance &#124; null &#62;',
    },
    {
      name: 'dropdownVisible',
      description: { cn: 'tooltip 显示状态', en: 'tooltip display status' },
      type: 'function',
      param: '() =&gt; ComputedRef&#60; boolean &#62;',
    },
  ],
};
export default doc;
