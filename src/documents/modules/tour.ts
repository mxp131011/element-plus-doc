import type { TagDoc } from '@/types/tag-doc';

const doc: TagDoc.TagDocInstance = {
  url: 'component/tour.html',
  attributes: [
    {
      name: 'show-arrow',
      description: { cn: '是否显示箭头', en: 'whether to show the arrow' },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'placement',
      description: {
        cn: '引导卡片相对于目标元素的位置',
        en: 'position of the guide card relative to the target element',
      },
      type: 'enum',
      default: 'bottom',
      value: ['top', 'top-start', 'top-end', 'bottom', 'bottom-start', 'bottom-end', 'left', 'left-start', 'left-end', 'right', 'right-start', 'right-end'],
    },
    {
      name: 'content-style',
      description: { cn: '为content自定义样式', en: 'custom style for content' },
      type: ['string', 'object', 'array', 'CSSProperties'],
      default: '-',
      value: [],
    },
    {
      name: 'mask',
      description: {
        cn: '是否启用遮罩，通过自定义属性改变遮罩样式以及填充的颜色',
        en: 'whether to enable masking, change mask style and fill color by pass custom props',
      },
      type: ['boolean', 'object'],
      default: 'true',
      value: [],
    },
    {
      name: 'type',
      description: { cn: '类型，影响底色与文字颜色', en: 'type, affects the background color and text color' },
      type: 'enum',
      default: 'default',
      value: ['default', 'primary'],
    },
    {
      name: 'model-value',
      description: { cn: '打开引导,支持双向绑定(`v-model`)', en: 'open tour. support `v-model`' },
      type: 'boolean',
      default: '',
      value: [],
    },
    {
      name: 'current',
      description: { cn: '当前值,支持双向绑定(`v-model:current`)', en: 'what is the current step. support `v-model:current`' },
      type: 'number',
      default: '',
      value: [],
    },
    {
      name: 'scroll-into-view-options',
      description: { cn: '是否支持当前元素滚动到视窗内，也可传入配置指定滚动视窗的相关参数', en: 'support pass custom scrollIntoView options' },
      type: ['boolean', 'object'],
      default: "{ block: 'center' }",
      value: [],
    },

    {
      name: 'z-index',
      description: {
        cn: 'Tour 的层级',
        en: "Tour's zIndex",
      },
      type: 'number',
      default: '2001',
      value: [],
    },
    {
      name: 'show-close',
      description: {
        cn: '是否显示关闭按钮',
        en: 'whether to show a close button',
      },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'close-icon',
      description: {
        cn: '自定义关闭图标，默认Close',
        en: 'custom close icon, default is Close',
      },
      type: ['string', 'Component'],
      default: '',
      value: [],
    },
    {
      name: 'close-on-press-escape',
      description: {
        cn: '是否可以通过按下 ESC 关闭引导',
        en: 'whether the Dialog can be closed by pressing ESC',
      },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'target-area-clickable',
      description: {
        cn: '启用蒙层时，target 元素区域是否可以点击。',
        en: 'whether the target element can be clickable, when using mask',
      },
      type: 'boolean',
      default: 'true',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: 'tourStep 组件列表', en: 'tourStep component list' },
    },
    {
      name: 'indicators',
      description: { cn: '自定义指示器, scope 参数是 { current, total }', en: 'custom indicator, The scope parameter is { current, total }' },
    },
  ],
  events: [
    {
      name: 'close',
      description: { cn: '关闭引导时的回调函数', en: 'callback function on shutdown' },
      type: 'function',
      param: '(current: number) =&gt; void',
    },
    {
      name: 'finish',
      description: { cn: '引导完成时的回调', en: 'callback function on finished' },
      type: 'function',
      param: '() =&gt; void',
    },
    {
      name: 'change',
      description: { cn: '步骤改变时的回调', en: 'callback when the step changes' },
      type: 'function',
      param: '(current: number) =&gt; void',
    },
  ],
};

export default doc;
