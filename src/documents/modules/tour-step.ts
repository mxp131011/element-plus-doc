import type { TagDoc } from '@/types/tag-doc';

const doc: TagDoc.TagDocInstance = {
  url: 'component/tour.html#tourstep-属性',
  attributes: [
    {
      name: 'target',
      description: {
        cn: '获取引导卡片指向的元素， 为空时居中于屏幕。 自 `2.5.2` 以来支持字符串和函数类型。 字符串类型是文档.querySelector的选择器。',
        en: 'get the element the guide card points to. Empty makes it show in center of screen. the string and Function types are supported since `2.5.2`. the string type is selectors of document.querySelector.',
      },
      type: [`HTMLElement`, `string`, `function`],
      default: '',
      value: [],
    },
    {
      name: 'show-arrow',
      description: {
        cn: '是否显示箭头',
        en: 'whether to show the arrow',
      },
      type: 'boolean',
      default: '',
      value: [],
    },
    {
      name: 'title',
      description: { cn: '标题', en: 'title' },
      type: 'string',
      default: '-',
      value: [],
    },
    {
      name: 'description',
      description: { cn: '主要描述部分', en: 'description' },
      type: 'string',
      default: '-',
      value: [],
    },
    {
      name: 'placement',
      description: {
        cn: '引导卡片相对于目标元素的位置',
        en: 'position of the guide card relative to the target element',
      },
      type: 'enum',
      default: 'bottom',
      value: ['top', 'top-start', 'top-end', 'bottom', 'bottom-start', 'bottom-end', 'left', 'left-start', 'left-end', 'right', 'right-start', 'right-end'],
    },
    {
      name: 'content-style',
      description: { cn: '为content自定义样式', en: 'custom style for content' },
      type: 'CSSProperties',
      default: '',
      value: [],
    },
    {
      name: 'mask',
      description: {
        cn: '是否启用遮罩，通过自定义属性改变遮罩样式以及填充的颜色',
        en: 'whether to enable masking, change mask style and fill color by pass custom props',
      },
      type: ['boolean', 'object'],
      default: 'true',
      value: [],
    },
    {
      name: 'type',
      description: { cn: '类型，影响底色与文字颜色', en: 'type, affects the background color and text color' },
      type: 'enum',
      default: 'default',
      value: ['default', 'primary'],
    },
    {
      name: 'next-button-props',
      description: { cn: '“下一步”按钮的属性', en: 'properties of the Next button' },
      type: 'object',
      default: '',
      value: [],
    },
    {
      name: 'prev-button-props',
      description: { cn: '“上一步”按钮的属性', en: 'properties of the previous button' },
      type: 'object',
      default: '',
      value: [],
    },
    {
      name: 'scroll-into-view-options',
      description: {
        cn: '是否支持当前元素滚动到视窗内，也可传入配置指定滚动视窗的相关参数，默认跟随 `Tour` 的 `scrollIntoViewOptions` 属性',
        en: 'support pass custom `scrollIntoView` options, the default follows the `scrollIntoViewOptions` property of `Tour`',
      },
      type: ['boolean', 'object'],
      default: "{ block: 'center' }",
      value: [],
    },
    {
      name: 'show-close',
      description: {
        cn: '是否显示关闭按钮',
        en: 'whether to show a close button',
      },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'close-icon',
      description: {
        cn: '自定义关闭图标，默认Close',
        en: 'custom close icon, default is Close',
      },
      type: ['string', 'Component'],
      default: '',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '主要描述部分', en: 'description' },
    },
    {
      name: 'header',
      description: { cn: '头部', en: 'header' },
    },
  ],
  events: [
    {
      name: 'close',
      description: { cn: '关闭引导时的回调函数', en: 'callback function on shutdown' },
      type: 'function',
      param: '() =&gt; void',
    },
  ],
};

export default doc;
