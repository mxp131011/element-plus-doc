import type { TagDoc } from '@/types/tag-doc';
const doc: TagDoc.TagDocInstance = {
  url: 'component/anchor.html#anchorlink-attributes',
  attributes: [
    {
      name: 'title',
      description: { cn: '链接的文本内容。', en: 'the text content of the anchor link.' },
      type: 'string',
      default: '',
      value: [],
    },
    {
      name: 'href',
      description: { cn: '链接的地址。', en: 'The address of the anchor link.' },
      type: 'string',
      default: '',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '链接的内容', en: 'the content of the anchor link.' },
    },
    {
      name: 'sub-link',
      description: { cn: '子链接的槽位', en: 'slots for child links.' },
    },
  ],
};

export default doc;
