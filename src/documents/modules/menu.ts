import type { TagDoc } from '@/types/tag-doc';

const doc: TagDoc.TagDocInstance = {
  url: 'component/menu.html',
  attributes: [
    {
      name: 'mode',
      description: { cn: '菜单展示模式', en: 'menu display mode' },
      type: 'enum',
      default: "'vertical'",
      value: ['horizontal', 'vertical'],
    },
    {
      name: 'collapse',
      description: {
        cn: '是否水平折叠收起菜单（仅在 mode 为 vertical 时可用）',
        en: 'whether the menu is collapsed (available only in vertical mode)',
      },
      type: 'boolean',
      default: 'false',
      value: [],
    },
    {
      name: 'ellipsis',
      description: {
        cn: '是否省略多余的子项（仅在横向模式生效）',
        en: 'whether the menu is ellipsis (available only in horizontal mode)',
      },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'ellipsis-icon',
      description: {
        cn: '自定义省略图标 (仅在水平模式下可用)',
        en: 'custom ellipsis icon (available only in horizontal mode and ellipsis is true)',
      },
      type: [`string`, `Component`],
      default: '#ffffff',
      value: [],
    },
    {
      name: 'popper-offset',
      description: {
        cn: '弹出层的偏移量(对所有子菜单有效)',
        en: 'offset of the popper (effective for all submenus)',
      },
      type: 'number',
      default: '6',
      value: [],
    },

    {
      name: 'default-active',
      description: { cn: '页面加载时默认激活菜单的 index', en: 'index of active menu on page load' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'default-openeds',
      description: { cn: '默认打开的 sub-menu 的 index 的数组', en: 'array that contains indexes of currently active sub-menus' },
      type: 'array',
      default: '—',
      value: [],
    },
    {
      name: 'unique-opened',
      description: { cn: '是否只保持一个子菜单的展开', en: 'whether only one sub-menu can be active' },
      type: 'boolean',
      default: 'false',
      value: [],
    },
    {
      name: 'menu-trigger',
      description: {
        cn: '子菜单打开的触发方式，只在 mode 为 horizontal 时有效。',
        en: "how sub-menus are triggered, only works when `mode` is 'horizontal'",
      },
      type: 'enum',
      default: "'hover'",
      value: ['hover', 'click'],
    },
    {
      name: 'router',
      description: {
        cn: '是否启用 vue-router 模式。 启用该模式会在激活导航时以 index 作为 path 进行路由跳转 使用 default-active 来设置加载时的激活项。',
        en: "whether `vue-router` mode is activated. If true, index will be used as 'path' to activate the route action. Use with `default-active` to set the active item on load.",
      },
      type: 'boolean',
      default: 'false',
      value: [],
    },
    {
      name: 'collapse-transition',
      description: { cn: '是否开启折叠动画', en: 'whether to enable the collapse transition' },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'popper-effect',
      description: {
        cn: 'Tooltip 主题，内置了 `dark` / `light` 两种主题',
        en: 'Tooltip theme, built-in theme: `dark` / `light` when menu is collapsed',
      },
      type: 'enum',
      default: "'dark'",
      value: ['dark', 'light'],
    },
    {
      name: 'close-on-click-outside',
      description: { cn: '单击外部时是否折叠菜单', en: 'optional, whether menu is collapsed when clicking outside' },
      type: 'boolean',
      default: 'false',
      value: [],
    },
    {
      name: 'popper-class',
      description: { cn: '为 popper 添加类名', en: 'custom class name for all popup menus' },
      type: 'string',
      default: '',
      value: [],
    },
    {
      name: 'show-timeout',
      description: { cn: '菜单出现前的延迟', en: 'Control timeout for all menus before showing' },
      type: 'number',
      default: '300',
      value: [],
    },
    {
      name: 'hide-timeout',
      description: { cn: '菜单消失前的延迟', en: 'Control timeout for all menus before hiding' },
      type: 'number',
      default: '300',
      value: [],
    },
  ],
  events: [
    {
      name: 'select',
      description: { cn: '菜单激活回调', en: 'callback function when menu is activated' },
      type: 'function',
      param: 'index: 选中菜单项的 index, indexPath: 选中菜单项的 index path, item: 选中菜单项, routeResult: vue-router 的返回值（如果 router 为 true）',
    },
    {
      name: 'open',
      description: { cn: 'sub-menu 展开的回调', en: 'callback function when sub-menu expands' },
      type: 'function',
      param: 'index: 打开的 sub-menu 的 index, indexPath: 打开的 sub-menu 的 index path',
    },
    {
      name: 'close',
      description: { cn: 'sub-menu 收起的回调', en: 'callback function when sub-menu collapses' },
      type: 'function',
      param: 'index: 收起的 sub-menu 的 index, indexPath: 收起的 sub-menu 的 index path',
    },
  ],
  exposes: [
    {
      name: 'open',
      description: { cn: '展开指定的 sub-menu', en: 'open a specific sub-menu' },
      type: 'function',
      param: 'index: 需要打开的 sub-menu 的 index',
    },
    {
      name: 'close',
      description: { cn: '收起指定的 sub-menu', en: 'close a specific sub-menu' },
      type: 'function',
      param: 'index: 需要收起的 sub-menu 的 index',
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '自定义默认内容', en: 'customize default content' },
      subtags: ['SubMenu', 'Menu-Item', 'Menu-Item-Group'],
    },
  ],
};
export default doc;
