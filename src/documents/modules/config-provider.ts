import type { TagDoc } from '@/types/tag-doc';
const doc: TagDoc.TagDocInstance = {
  url: 'component/config-provider.html',
  attributes: [
    {
      name: 'locale',
      description: {
        cn: '翻译文本对象. ([languages](https://github.com/element-plus/element-plus/tree/dev/packages/locale/lang))',
        en: 'Locale Object. ([languages](https://github.com/element-plus/element-plus/tree/dev/packages/locale/lang))',
      },
      type: 'object',
      default: 'en',
      value: [],
    },
    {
      name: 'size',
      description: { cn: '全局组件大小', en: 'global component size' },
      type: 'enum',
      default: "'default'",
      value: ['large', 'default', 'small'],
    },
    {
      name: 'zIndex',
      description: { cn: '全局初始化 zIndex 的值', en: 'global Initial zIndex' },
      type: 'number',
      default: '—',
      value: [],
    },
    {
      name: 'namespace',
      description: {
        cn: '全局组件类名称前缀 (需要配合 [$namespace](https://github.com/element-plus/element-plus/blob/dev/packages/theme-chalk/src/mixins/config.scss#L1) 使用)',
        en: 'global component className prefix (cooperated with [$namespace](https://github.com/element-plus/element-plus/blob/dev/packages/theme-chalk/src/mixins/config.scss#L1))',
      },
      type: 'string',
      default: 'el',
      value: [],
    },
    {
      name: 'button',
      description: {
        cn: '按钮相关的配置[详见下表](https://cn.element-plus.org/zh-CN/component/config-provider.html#button-attribute)，ts类型：{autoInsertSpace?: boolean}',
        en: 'button related configuration, [see the following table](https://cn.element-plus.org/zh-CN/component/config-provider.html#button-attribute) ts type：{autoInsertSpace?: boolean}',
      },
      type: 'object',
      default: '—',
      value: [],
    },
    {
      name: 'message',
      description: {
        cn: '消息相关配置 [详见下表](https://cn.element-plus.org/zh-CN/component/config-provider.html#message-attribute)，ts类型：{max?: number; grouping?:boolean; duration?:number; showClose?:boolean; offset?:number}',
        en: 'message related configuration, [see the following table](https://cn.element-plus.org/zh-CN/component/config-provider.html#message-attribute) ( ts type: {max?: number; grouping?:boolean; duration?:number; showClose?:boolean; offset?:number})',
      },
      type: 'object',
      default: '—',
      value: [],
    },
    {
      name: 'experimental-features',
      description: {
        cn: '将要添加的实验阶段的功能，所有功能都是默认设置为 false',
        en: 'features at experimental stage to be added, all features are default to be set to false',
      },
      type: 'object',
      default: '—',
      value: [],
    },
    {
      name: 'empty-values',
      description: {
        cn: '输入类组件空值',
        en: 'global empty values of components',
      },
      type: 'array',
      default: '—',
      value: [],
    },
    {
      name: 'value-on-clear',
      description: {
        cn: '输入类组件清空值',
        en: 'global clear return value',
      },
      type: ['object', 'number', 'boolean', 'function'],
      default: '—',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '自定义默认内容', en: 'customize default content' },
    },
  ],
  childAttributes: [
    {
      name: 'button',
      list: [
        {
          name: 'autoInsertSpace',
          description: {
            cn: '自动在两个中文字符之间插入空格',
            en: 'automatically insert a space between two chinese characters(this will only take effect when the text length is 2 and all characters are in Chinese.)',
          },
          type: 'boolean',
          default: 'false',
          value: [],
        },
      ],
    },
    {
      name: 'message',
      list: [
        {
          name: 'max',
          description: { cn: '可同时显示的消息最大数量', en: 'the maximum number of messages that can be displayed at the same time' },
          type: 'number',
          default: '—',
          value: [],
        },
        {
          name: 'grouping',
          description: {
            cn: '合并内容相同的消息，不支持 VNode 类型的消息',
            en: 'merge messages with the same content, type of VNode message is not supported',
          },
          type: 'number',
          default: '—',
          value: [],
        },
        {
          name: 'duration',
          description: {
            cn: '显示时间，单位为毫秒。 设为 0 则不会自动关闭',
            en: 'display duration, millisecond. If set to 0, it will not turn off automatically',
          },
          type: 'boolean',
          default: '—',
          value: [],
        },
        {
          name: 'showClose',
          description: { cn: '是否显示关闭按钮', en: 'whether to show a close button' },
          type: 'boolean',
          default: '—',
          value: [],
        },
        {
          name: 'offset',
          description: { cn: 'Message 距离窗口顶部的偏移量', en: 'set the distance to the top of viewport' },
          type: 'number',
          default: '—',
          value: [],
        },
      ],
    },
  ],
};
export default doc;
