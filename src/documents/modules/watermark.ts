import type { TagDoc } from '@/types/tag-doc';

const doc: TagDoc.TagDocInstance = {
  url: 'component/watermark.html',
  attributes: [
    {
      name: 'width',
      description: { cn: '水印的宽度， `content` 的默认值是它自己的宽度', en: 'The width of the watermark, the default value of `content` is its own width' },
      type: 'number',
      default: '120',
      value: [],
    },
    {
      name: 'height',
      description: {
        cn: '水印的高度， `content` 的默认值是它自己的高度',
        en: 'The height of the watermark, the default value of `content` is its own height',
      },
      type: 'number',
      default: '64',
      value: [],
    },
    {
      name: 'rotate',
      description: { cn: '水印的旋转角度', en: 'When the watermark is drawn, the rotation Angle' },
      type: 'number',
      default: '-22',
      value: [],
    },
    {
      name: 'zIndex',
      description: { cn: '水印元素的z-index值', en: 'The z-index of the appended watermark element' },
      type: 'number',
      default: '9',
      value: [],
    },
    {
      name: 'image',
      description: { cn: '水印图片，建议使用 2x 或 3x 图像', en: 'Image source, it is recommended to export 2x or 3x image, high priority' },
      type: 'string',
      default: '',
      value: [],
    },
    {
      name: 'content',
      description: { cn: '水印文本内容', en: 'Watermark text content' },
      type: ['string', 'array'],
      default: "'center'",
      value: [],
    },
    {
      name: 'font',
      description: { cn: '文字样式，具体看下表', en: 'Text style. see the following table' },
      type: 'object',
      default: '',
      value: [],
    },
    {
      name: 'gap',
      description: { cn: '水印之间的间距', en: 'The spacing between watermarks' },
      type: 'array',
      default: '[100, 100]',
      value: [],
    },
    {
      name: 'offset',
      description: {
        cn: '水印从容器左上角的偏移 默认值为 gap/2',
        en: 'The offset of the watermark from the upper left corner of the container. The default is gap/2',
      },
      type: 'array',
      default: '[[gap[0]/2, gap[1]/2]]',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '添加水印的容器', en: 'container for adding watermark' },
    },
  ],
  childAttributes: [
    {
      name: 'Font',
      list: [
        {
          name: 'color',
          description: { cn: '字体颜色', en: 'font color' },
          type: 'string',
          default: 'rgba(0,0,0,.15)',
          value: [],
        },
        {
          name: 'fontSize',
          description: { cn: '字体颜色', en: 'font size' },
          type: 'number',
          default: '16',
          value: [],
        },
        {
          name: 'fontWeight',
          description: { cn: '字重', en: 'font weight' },
          type: ['enum', 'number'],
          default: 'normal',
          value: ['normal', 'light', 'weight'],
        },
        {
          name: 'fontFamily',
          description: { cn: '字体', en: 'font family' },
          type: ['string'],
          default: 'sans-serif',
          value: [],
        },
        {
          name: 'fontStyle',
          description: { cn: '字体样式', en: 'font style' },
          type: ['enum'],
          default: 'normal',
          value: ['none', 'normal', 'italic', 'oblique'],
        },
        {
          name: 'textAlign',
          description: { cn: '文本对齐', en: 'text align' },
          type: ['enum'],
          default: 'center',
          value: ['left', 'right', 'center', 'start', 'end'],
        },
        {
          name: 'textBaseline',
          description: { cn: '文本基线', en: 'text baseline' },
          type: ['enum'],
          default: 'top',
          value: ['top', 'hanging', 'middle', 'alphabetic', 'ideographic', 'bottom'],
        },
      ],
    },
  ],
};

export default doc;
