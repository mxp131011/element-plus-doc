import type { TagDoc } from '@/types/tag-doc';
const doc: TagDoc.TagDocInstance = {
  url: 'component/collapse.html#collapse-item-attributes',
  attributes: [
    {
      name: 'name',
      description: { cn: '唯一标志符', en: 'unique identification of the panel' },
      type: ['string', 'number'],
      default: '—',
      value: [],
    },
    {
      name: 'title',
      description: { cn: '面板标题', en: 'title of the panel' },
      type: 'string',
      default: '—',
      value: [],
    },
    {
      name: 'icon',
      description: { cn: '折叠项目的图标', en: 'icon of the collapse item' },
      type: ['string', 'Component'],
      default: 'ArrowRight',
      value: [],
    },
    {
      name: 'disabled',
      description: { cn: '是否禁用', en: 'disable the collapse item' },
      type: 'boolean',
      default: '—',
      value: [],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: '折叠项内容', en: 'content of Collapse Item' },
    },
    {
      name: 'title',
      description: { cn: '折叠项的标题', en: 'content of Collapse Item title' },
    },
    {
      name: 'icon',
      description: { cn: '折叠项目图标的内容,会返回isActive属性', en: 'content of Collapse Item icon' },
    },
  ],
};
export default doc;
