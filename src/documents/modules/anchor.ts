import type { TagDoc } from '@/types/tag-doc';
const doc: TagDoc.TagDocInstance = {
  url: 'component/anchor.html#anchor-api',
  attributes: [
    {
      name: 'container',
      description: { cn: '滚动的容器', en: 'scroll container.' },
      type: ['string', 'HTMLElement', 'Window'],
      default: '',
      value: [],
    },
    {
      name: 'offset',
      description: {
        cn: '设置锚点滚动的偏移量',
        en: 'set the offset of the anchor scroll.',
      },
      type: 'number',
      default: '0',
      value: [],
    },
    {
      name: 'bound',
      description: { cn: '触发锚点的元素的位置偏移量', en: 'the offset of the element starting to trigger the anchor.' },
      type: 'number',
      default: '15',
      value: [],
    },
    {
      name: 'duration',
      description: { cn: '设置容器滚动持续时间，单位为毫秒。', en: 'set the scroll duration of the container, in milliseconds.' },
      type: 'number',
      default: '300',
      value: [],
    },
    {
      name: 'marker',
      description: { cn: '是否显示标记', en: 'whether to show the marker.' },
      type: 'boolean',
      default: 'true',
      value: [],
    },
    {
      name: 'type',
      description: { cn: '设置锚点类型', en: 'set Anchor type.' },
      type: 'enum',
      default: 'default',
      value: ['defalut', 'underline'],
    },
    {
      name: 'direction',
      description: { cn: '设置锚点方向', en: 'Set Anchor direction.' },
      type: 'enum',
      default: 'vertical',
      value: ['vertical', 'horizontal'],
    },
  ],
  slots: [
    {
      name: 'default',
      description: { cn: 'AnchorLink 组件列表', en: 'AnchorLink component list' },
    },
  ],
  events: [
    {
      name: 'change',
      description: { cn: 'step 改变时的回调', en: 'callback when the step changes' },
      type: 'function',
      param: '(href: string) =&gt; void',
    },
    {
      name: 'click',
      description: { cn: '当用户点击链接时触发', en: 'Triggered when the user clicks on the link' },
      type: 'function',
      param: '(e: MouseEvent, href?: string) =&gt; void',
    },
  ],
  exposes: [
    {
      name: 'scrollTo',
      description: { cn: '手动滚动到特定位置', en: 'Manually scroll to the specific position.' },
      type: 'function',
      param: '(href: string) =&gt; void',
    },
  ],
};

export default doc;
